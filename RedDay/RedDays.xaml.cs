﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using RedDay.Business;

namespace RedDay
{
    public partial class RedDays : PhoneApplicationPage
    {
        public RedDays()
        {
            InitializeComponent();
        }

        private void btn_RedDay_Click(object sender, RoutedEventArgs e)
        {
            //if selected a day;
            if (cal_RedDay.SelectedValue.HasValue)
            {
                bn_Information bn_inf = new bn_Information();
                bn_inf.CreateRedDay(cal_RedDay.SelectedValue.Value);

                string navigate = "/MainPage.xaml";
                navigate += String.Format("?redDay={0}",
                    cal_RedDay.SelectedValue.Value.ToShortDateString());
                NavigationService.Navigate(new Uri(navigate, UriKind.Relative));
            }
        }

        private void cal_RedDay_SelectedValueChanged(object sender, Telerik.Windows.Controls.ValueChangedEventArgs<object> e)
        {
            var selectedDay = cal_RedDay.SelectedValue;
            txt_SelectedDay.Text = selectedDay.Value.ToShortDateString();
        }
    }
}