﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RedDay.Data;

namespace RedDay.Business
{
    public class bn_Reference
    {
        public const string AmountOfRunning = "AmountOfRunning";
        private string _connection = @"Data Source=isostore:/pr_wetdays.sdf";
        private WetDaysDataContext db;

        public bn_Reference(string connection = null)
        {
            if (connection != null) _connection = connection;

            db = new WetDaysDataContext(_connection);

            if (db.DatabaseExists() == false)
            {
                db.CreateDatabase();
            }
        }

        public pr_Reference GetByKey(string key)
        {
            var result = (
                    from get in db.pr_References
                    where get.Key == key
                    select get
                ).ToList();

            if (result.Count > 0)
                return result.First();
            else
                return null;
        }
        public bool IsExists(string key)
        {
            var result = GetByKey(key);

            if (result != null)
                return true;
            else
                return false;
        }

        public void Create(string key, string value)
        {
            var result = (
                from get in db.pr_References
                where get.Key == key
                select get)
            .ToList();

            if (result.Count == 0)
            {
                db.pr_References.InsertOnSubmit(new pr_Reference
                {
                    Key = key,
                    Value = value
                });

                db.SubmitChanges();
            }
        }
        public void Update(string key, string value)
        {
            if (IsExists(key))
            {
                Delete(key);
            }
            Create(key, value);
        }
        public void Delete(string key)
        {
            if (IsExists(key))
            {
                db.pr_References.DeleteOnSubmit(GetByKey(key));
                db.SubmitChanges();
            }
        }
    }
}
