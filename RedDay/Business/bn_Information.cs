﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RedDay.Data;

namespace RedDay.Business
{
    public class bn_Information
    {
        private string _connection = @"Data Source=isostore:/pr_wetdays.sdf";
        private WetDaysDataContext db;

        public const string WetDays = "wetdays";
        public const string RedDays = "reddays";
        public const string NormalDays = "normaldays";

        public bn_Information(string connection = null)
        {
            if (connection != null) _connection = connection;

            db = new WetDaysDataContext(_connection);

            if (db.DatabaseExists() == false)
            {
                db.CreateDatabase();
            }
        }

        public pr_Information GetById(Guid informationId)
        {
            var result = (
                    from get in db.pr_Informations
                    where get.InformationId == informationId
                    select get
                ).ToList();

            if (result.Count > 0)
                return result.First();
            else
                return null;
        }
        public pr_Information GetNewestDay(DateTime date, string type = RedDays)
        {
            var result = (
                    from get in db.pr_Informations
                    where
                        get.Type == type &&
                        get.Date.Date <= date.Date
                    orderby date descending
                    select get
                ).Take(1).Single();

            return result;
        }
        public List<pr_Information> GetByDay(DateTime date)
        {
            var result = (
                    from get in db.pr_Informations
                    where
                        get.Date.Date == date.Date
                    orderby get.Date ascending
                    select get
                ).ToList();

            return result;
        }

        public bool IsExists(Guid informationId)
        {
            var result = GetById(informationId);

            if (result != null)
                return true;
            else
                return false;
        }
        public void Create(pr_Information data)
        {
            if (data.Description == null) data.Description = "";
            if (data.Type == null) data.Type = NormalDays;

            db.pr_Informations.InsertOnSubmit(data);
            db.SubmitChanges();
        }
        public void Update(pr_Information data)
        {
            if (IsExists(data.InformationId))
            {
                Delete(data.InformationId);
            }

            Create(data);
        }
        public void Delete(Guid informationId)
        {
            if (IsExists(informationId))
            {
                db.pr_Informations.DeleteOnSubmit(GetById(informationId));
                db.SubmitChanges();
            }
        }

        public void CreateRedDay(DateTime date)
        {
            var data = new pr_Information();
            data.InformationId = Guid.NewGuid();
            data.Date = new DateTime(date.Year, date.Month, date.Day);
            data.Type = RedDays;

            Create(data);
        }
        public void CreateWetDay(DateTime date)
        {
            var data = new pr_Information();
            data.InformationId = Guid.NewGuid();
            data.Date = new DateTime(date.Year, date.Month, date.Day);
            data.Type = WetDays;

            Create(data);
        }
    }
}
