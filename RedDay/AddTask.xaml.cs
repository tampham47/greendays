﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.ComponentModel;
using RedDay.Business;
using RedDay.Data;

namespace RedDay
{
    public partial class AddTask : PhoneApplicationPage
    {
        public AddTask()
        {
            InitializeComponent();
        }

        //lấy tham số được truyền vào
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back) return;

            string str_selectedDate = "";
            DateTime redDay;
            if (NavigationContext.QueryString.TryGetValue("selectedDate", out str_selectedDate))
            {
                if (str_selectedDate != null || str_selectedDate != "")
                {
                    redDay = DateTime.Parse(str_selectedDate);
                    datePicker.Value = redDay;
                }
                else
                {
                    datePicker.Value = DateTime.Now;
                }
            }
        }

        private void buttonCancel_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string navigate = "/MainPage.xaml";
            NavigationService.Navigate(new Uri(navigate, UriKind.Relative));
        }

        private void addTask_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            bn_Information bn_info = new bn_Information();
            pr_Information data = new pr_Information();

            if (datePicker.Value.HasValue)
                data.Date = datePicker.Value.Value;
            else
                data.Date = DateTime.Now;
            data.InformationId = Guid.NewGuid();
            data.Description = txtDescription.Text;

            bn_info.Create(data);

            string navigate = "/MainPage.xaml";
            NavigationService.Navigate(new Uri(navigate, UriKind.Relative));
        }
    }

    public class Model : INotifyPropertyChanged
    {
        public List<TaskType> TaskTypes { get; private set; }

        public Model()
        {
            TaskTypes = new List<TaskType>();
            TaskTypes.Add(new TaskType
            {
                Type = "Normal Day",
                Color = "Green"
            });
            TaskTypes.Add(new TaskType
            {
                Type = "Red Day",
                Color = "Red"
            });
            TaskTypes.Add(new TaskType
            {
                Type = "Wet Day",
                Color = "Blue"
            });
        }

        private void InvokePropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class TaskType
    {
        public string Color { get; set; }
        public string Type { get; set; }
    }
}