﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.IO.IsolatedStorage;
using System.IO;
using System.Windows.Resources;
using System.Windows.Navigation;

using RedDay.Business;
//using RedDay.ViewModels;

namespace RedDay
{
    public partial class MainPage : PhoneApplicationPage
    {
        private const string connectionString = @"Data Source=isostore:/pr_wetdays.sdf";

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;
            this.Loaded += new RoutedEventHandler(MainPage_Loaded);

            bn_Information bn_info = new bn_Information();
            list_Details.ItemsSource = bn_info.GetByDay(DateTime.Now);
        }

        //lấy tham số được truyền vào
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back) return;

            string str_RedDay = "";
            DateTime redDay;
            if (NavigationContext.QueryString.TryGetValue("redDay", out str_RedDay))
            {
                if (str_RedDay != null || str_RedDay != "")
                {
                    redDay = DateTime.Parse(str_RedDay);
                    WeekendDaySelector.LastRedDay = redDay;
                }
                else
                {
                    bn_Information bn_info = new bn_Information();
                    var date = bn_info.GetNewestDay(DateTime.Now);
                    WeekendDaySelector.LastRedDay = date.Date;
                }
            }
        }

        // Load data for the ViewModel Items
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }
        }

        private void ApplicationBarIconButton_Click_Add(object sender, EventArgs e)
        {
            string navigate = "/AddTask.xaml";

            if (cal_GreenDays.SelectedValue.HasValue)
            {
                navigate += String.Format("?selectedDate={0}",
                    cal_GreenDays.SelectedValue.Value.ToShortDateString());
            }
            NavigationService.Navigate(new Uri(navigate, UriKind.Relative));
        }

        private void ApplicationBarIconButton_Click_Details(object sender, EventArgs e)
        {

        }
    }
}