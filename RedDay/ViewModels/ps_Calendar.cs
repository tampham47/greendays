﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Calendar;

namespace RedDay
{
    public class WeekendDaySelector : DataTemplateSelector
    {
        public static DateTime LastRedDay = DateTime.Now.AddDays(-28);

        public DataTemplate RedDayTemplate { get; set; }
        public DataTemplate WetDayTemplate { get; set; }
        public DataTemplate RedTemplate { get; set; }
        public DataTemplate BlueTemplate { get; set; }
        public DataTemplate GreenTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            CalendarButtonContentInfo info = item as CalendarButtonContentInfo;
            CalendarButton button = container as CalendarButton;

            if (info.Date == null) return null;
            if (info.Date.Value.Date < LastRedDay.Date) return null;

            if (info.Date.Value.Date == LastRedDay.Date) return RedDayTemplate;
            if (info.Date.Value.Date == LastRedDay.Date.AddDays(29).Date) return RedDayTemplate;

            int beginRisk = 9, endRisk = 19, distance;
            TimeSpan total = info.Date.Value - LastRedDay;
            distance = (int)total.TotalDays;

            if (distance > 32) return null;

            if (13 <= distance && distance <= 16) return WetDayTemplate;

            if (distance < beginRisk)
                return GreenTemplate;
            else
                if (distance < endRisk)
                    return RedTemplate;
                else
                    return GreenTemplate;
        }
    }
}
