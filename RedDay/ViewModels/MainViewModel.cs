﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using RedDay.Data;

namespace RedDay
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            this.Items = new ObservableCollection<ItemViewModel>();
            this.GuideItems = new ObservableCollection<ps_GuideItem>();
            this.TaskList = new ObservableCollection<pr_Information>();
        }

        /// <summary>
        /// A collection for ItemViewModel objects.
        /// </summary>
        public ObservableCollection<ItemViewModel> Items { get; private set; }
        public ObservableCollection<ps_GuideItem> GuideItems { get; private set; }
        public ObservableCollection<pr_Information> TaskList { get; private set; }

        private string _sampleProperty = "Sample Runtime Property Value";
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding
        /// </summary>
        /// <returns></returns>
        public string SampleProperty
        {
            get
            {
                return _sampleProperty;
            }
            set
            {
                if (value != _sampleProperty)
                {
                    _sampleProperty = value;
                    NotifyPropertyChanged("SampleProperty");
                }
            }
        }

        public bool IsDataLoaded
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates and adds a few ItemViewModel objects into the Items collection.
        /// </summary>
        public void LoadData()
        {
            // Sample data; replace with real data
            this.Items.Add(new ItemViewModel()
            {
                LineOne = "Facilisi faucibus",
                LineTwo = "Những ngày đèn đỏ của những tháng trước",
                LineThree = "Facilisi faucibus habitant inceptos interdum lobortis nascetur pharetra placerat pulvinar sagittis senectus sociosqu"
            });
            this.Items.Add(new ItemViewModel()
            {
                LineOne = "Facilisi faucibus",
                LineTwo = "Những ngày đèn đỏ dự kiến của tháng tiếp theo, bạn có thể thay đổi những ngày này",
                LineThree = "Suscipit torquent ultrices vehicula volutpat maecenas praesent accumsan bibendum dictumst eleifend facilisi faucibus"
            });
            this.Items.Add(new ItemViewModel()
            {
                LineOne = "Facilisi faucibus",
                LineTwo = "Những ngày quan hệ an toàn",
                LineThree = "Habitant inceptos interdum lobortis nascetur pharetra placerat pulvinar sagittis senectus sociosqu suscipit torquent"
            });
            this.Items.Add(new ItemViewModel()
            {
                LineOne = "Facilisi faucibus",
                LineTwo = "Những ngày quan hệ không an toàn",
                LineThree = "Ultrices vehicula volutpat maecenas praesent accumsan bibendum dictumst eleifend facilisi faucibus habitant inceptos"
            });
            this.Items.Add(new ItemViewModel()
            {
                LineOne = "Facilisi faucibus",
                LineTwo = "Những ngày ẩm ướt trong chu kỳ kinh",
                LineThree = "Maecenas praesent accumsan bibendum dictumst eleifend facilisi faucibus habitant inceptos interdum lobortis nascetur"
            });

            this.GuideItems.Add(new ps_GuideItem() {
                ImagePath = "/RedDay_Data/Images/Red/Butterfly.png",
                Description = "Những ngày đèn đỏ của những tháng trước"
            });
            this.GuideItems.Add(new ps_GuideItem()
            {
                ImagePath = "/RedDay_Data/Images/Red/Bow.png",
                Description = "Những ngày đèn đỏ dự kiến của tháng tiếp theo, bạn có thể thay đổi những ngày này"
            });
            this.GuideItems.Add(new ps_GuideItem()
            {
                ImagePath = "/RedDay_Data/Images/Green/Location-Pin.png",
                Description = "Những ngày quan hệ an toàn"
            });
            this.GuideItems.Add(new ps_GuideItem()
            {
                ImagePath = "/RedDay_Data/Images/Red/Location-Pin.png",
                Description = "Những ngày quan hệ không an toàn"
            });
            this.GuideItems.Add(new ps_GuideItem()
            {
                ImagePath = "/RedDay_Data/Images/Blue/Cloud-Rain.png",
                Description = "Những ngày ẩm ướt trong chu kỳ kinh"
            });

            this.IsDataLoaded = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}